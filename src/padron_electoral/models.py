from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator, RegexValidator
from django.db import models

# Create your models here.


class Distelec(models.Model):
    code = models.CharField(validators=[MinLengthValidator(6),
                                        RegexValidator(regex=r"^[0-9]{6}$")],
                            max_length=6)
    province = models.CharField(max_length=200)
    canton = models.CharField(max_length=200)
    district = models.CharField(max_length=200)

    def __str__(self):
        return self.code


class Council(models.Model):
    code = models.CharField(verbose_name="Council code", max_length=5)
    name = models.CharField(max_length=200,
                            verbose_name="Council name")

    def __str__(self):
        return self.name


class Padron(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                verbose_name="User")

    codelec = models.ForeignKey(Distelec, on_delete=models.CASCADE)

    identity_card = models.CharField(max_length=9,
                                     validators=[RegexValidator(regex=r"^[0-9]{9}$")])
    sex = models.CharField(max_length=1,
                           validators=[RegexValidator(regex=r"^[af]{1}$")])
    exp_date = models.CharField(max_length=8,
                                validators=[RegexValidator(regex=r"^[0-9]{8}$")])
    council = models.ForeignKey(Council, on_delete=models.CASCADE)


    def __str__(self):
        name = self.user.get_full_name()
        if not name:
            name = self.user.username
        return name

