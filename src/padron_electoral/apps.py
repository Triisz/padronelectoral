from django.apps import AppConfig


class PadronElectoralConfig(AppConfig):
    name = 'padron_electoral'
