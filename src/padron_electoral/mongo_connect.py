from django.conf import settings
from pymongo import MongoClient

class MyMongo:
    def __init__(self):

        client = MongoClient(settings.MONGOSERVER,
                             username=settings.MONGOUSERNAME,
                             password=settings.MONGOPASSWORD
                             )
        self.db_name = settings.MONMONGODATABASE
        self.database = client.db_name
        self.distelec = self.database.distelec
        self.council = self.database.council
        self.padron = self.database.padron

    def save_distelec(self, *args, **kwargs):
        self.distelec.insert_one(kwargs['obj'])

    def save_council(self, *args, **kwargs):
        self.council.insert_one(kwargs['obj'])

    def save_padron(self, *args, **kwargs):
        self.padron.insert_one(kwargs['obj'])

"""def save(self, obj):
    if self.collection.find({'id': obj.id}).count():
        self.collection.update({"id": obj.id}, {'id': 123, 'name': 'test'})
    else:
        self.collection.insert_one({'id': 123, 'name': 'test'})"""
