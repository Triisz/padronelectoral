from django.conf import settings
from django.contrib.auth.models import User
from django.core.management import BaseCommand

from padron_electoral.mongo_connect import MyMongo
from src.padron_electoral.models import Distelec, Council, Padron


def read_file_codes(distelec_path):
    with open(distelec_path, 'r') as file:
        for line in file.readlines():
            obj = line.split(',')
            dict_object = {'code': obj[0].strip(),
                           'province': obj[1].strip(),
                           'canton': obj[2].strip(),
                           'district': obj[3].strip()
                           }

            yield dict_object


def read_file_padron(padron_path):
    with open(padron_path, 'r') as file:
        for line in file.readlines():
            obj = line.split(',')
            dict_object = {'identity_card': obj[0].strip(),
                           'codelec': obj[1].strip(),
                           'sex': obj[2].strip(),
                           'exp_date': obj[3].strip(),
                           'council': obj[4].strip(),
                           'name': obj[5].strip(),
                           'first_last_name': obj[6].strip(),
                           'second_last_name': obj[7].strip(),
                           }

            yield dict_object


def import_distelec(distelec_path):
    for obj in read_file_codes(distelec_path):
        if settings.DB == 'psql':
            new_obj = Distelec(code=obj['code'],
                               province=obj['province'],
                               canton=obj['canton'],
                               district=obj['disctrict'],
                               )
            new_obj.save()
        else:
            new_obj = MyMongo.save_distelec(obj=obj)
        yield new_obj


def import_psql_padron(distelec_path, padron_path):
    distelec_list = import_distelec(distelec_path)
    council_list = []
    user_list = []
    flag1, flag2 = True
    for obj in read_file_padron(padron_path):
        distelec_obj = filter(lambda x: x.code == obj['code'],
                              distelec_list)  # (x for x in distelec if x.code == obj['code'])

        new_padron = Padron(identity_card=obj['identity_card'],
                            sex=obj['sex'],
                            exp_date=obj['exp_date'],
                            )

        if distelec_obj:
            new_padron.codelec = distelec_obj,
        else:
            flag1 = False

        if obj['identity_card'] not in user_list:
            new_user = User(username=obj['name'],
                            password=obj['identity_card'],
                            first_name=obj['name'],
                            last_name=obj['first_last_name'] + ' ' + obj['second_last_name'])
            new_user.save()
            user_list.append(obj['identity_card'])
            new_padron.user = new_user
        else:
            flag2 = False

        if obj['council'] not in council_list:
            new_council = Council(code=obj['council'])
            new_council.save()
            new_padron.council = new_council
            council_list.append(obj['council'])
        else:
            new_padron.council = obj['council']

        if flag1 and flag2:
            new_padron.save()
        else:
            # log
            pass


def import_mongo_padron(distelec_path, padron_path):
    distelec_list = import_distelec(distelec_path)
    council_list = []
    user_list = User.objects.all()
    flag1, flag2 = True
    for obj in read_file_padron(padron_path):
        distelec_obj = filter(lambda x: x.code == obj['code'],
                              distelec_list)  # (x for x in distelec if x.code == obj['code'])

        new_padron = {'identity_card': obj['identity_card'],
                      'sex': obj['sex'],
                      'exp_date': obj['exp_date']
                      }

        if distelec_obj:
            new_padron['codelec'] = distelec_obj,
        else:
            flag1 = False
            # log

        find_user = user_list.filter(username=obj['name'],
                                     last_name=obj['first_last_name'] + ' ' + obj['second_last_name']).first()
        if not find_user:
            new_user = User(username=obj['name'],
                            password=obj['identity_card'],
                            first_name=obj['name'],
                            last_name=obj['first_last_name'] + ' ' + obj['second_last_name'])
            new_user.save()
            user_list.append(new_user)
            new_padron['name'] = obj['name']
            new_padron['first_last_name'] = obj['first_last_name']
            new_padron['second_last_name'] = obj['second_last_name']
        else:
            flag2 = False
            # log

        find_council = council_list.filter(code=obj['council']).first
        if not find_council:
            council_obj = {'code': obj['council']}
            new_council = MyMongo.save_council(obj=council_obj)
            new_padron['council'] = new_council['_id']
            council_list.append(new_council)
        else:
            new_padron.council = find_council['_id']

        if flag1 and flag2:
            MyMongo.save_padron(obj=new_padron)
        else:
            # log
            pass


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('distelec', type=str, help='Path of Distelec.txt file in your computer')
        parser.add_argument('padron', type=str, help='Path of Padron.txt file in your computer')

    def handle(self, *args, **kwargs):
        if settings.DB == 'psql':
            import_psql_padron(kwargs['distelec'], kwargs['padron'])
        else:
            import_mongo_padron(kwargs['distelec'], kwargs['padron'])
